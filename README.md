# Final Project

The goal of this project was to display a number of skills learned over the course of the bootcamp and to act as a pentester and a SOC analyst. 

Skills demonstrated included:

- Find vulnerabilities and exploits to gain access to a vulnerable site/server.
- Use Wireshark to analyze and find malicious traffic.
- Create a team presentation to show the analysis, assessment and hardening of a vulnerable system.
- Implementing alarms and thresholds.

The [Offensive](https://gitlab.com/grantklitzke1/final-project/-/blob/main/OffensiveReport.md), [Defensive](https://gitlab.com/grantklitzke1/final-project/-/blob/main/DefensiveReport.md) and [NetworkAnalysis](https://gitlab.com/grantklitzke1/final-project/-/blob/main/NetworkAnalysis.md) reports will show all the work.
